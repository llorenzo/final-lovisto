from django.shortcuts import redirect,render
from django.http import HttpResponse, Http404
from django.contrib.auth import logout
from .models import Aportacion, Comentario, Voto
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.utils import timezone
from django.http import JsonResponse
from .forms import ComentarioForm
import xmltodict
import urllib.request
import requests
import json
from bs4 import BeautifulSoup

def autenticar(request):
	if request.method == "POST":
		action = request.POST['action']
		if action == 'Logout':
			logout(request)
			
		if action == 'Login':
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(request, username=username, password=password)
			if user is not None:
				login(request, user)

def votarPositivo(request, username, aportacion):
	votado = False

	lista_votos = Voto.objects.all().filter(aportacion=aportacion)
	for voto in lista_votos:
		if voto.user == username:
			votado = True
			if voto.opcion != 'P':
				aportacion.num_votos_positivos = aportacion.num_votos_positivos + 1
				aportacion.num_votos_negativos = aportacion.num_votos_negativos - 1
				voto.opcion = 'P'
				aportacion.save()
				voto.save()
	if not votado:
		aportacion.num_votos_positivos = aportacion.num_votos_positivos + 1
		nuevoVoto= Voto(aportacion=aportacion, user=username, opcion='P')
		nuevoVoto.save()
		aportacion.save()

def votarNegativo(request, username, aportacion):
	votado = False

	lista_votos = Voto.objects.all().filter(aportacion=aportacion)
	for voto in lista_votos:
		if voto.user == username:
			votado = True
			if voto.opcion != 'N':
				aportacion.num_votos_positivos = aportacion.num_votos_positivos - 1
				aportacion.num_votos_negativos = aportacion.num_votos_negativos + 1
				voto.opcion = 'N'
				aportacion.save()
				voto.save()
	if not votado:
		aportacion.num_votos_negativos = aportacion.num_votos_negativos + 1
		nuevoVoto= Voto(aportacion=aportacion, user=username, opcion='N')
		nuevoVoto.save()
		aportacion.save()	
				
#Función para gestionar página de todas las aportaciones	
@csrf_exempt
def aportaciones(request):
	autenticar(request)
	modo = 0
	
	if request.method == "POST":
		action = request.POST['action']
		if action == "Claro":
			modo = 0
		if action == "Oscuro":
			modo = 1
	
	if request.GET.get("format")=="xml/":
		template = loader.get_template('LoVisto/aportaciones.xml')
		context = {
			'lista_aportaciones': Aportacion.objects.all(),
		}
		return HttpResponse(template.render(context, request))
		
	if request.GET.get("format")=="json/":
		queryset = Aportacion.objects.filter().values()
		return JsonResponse({"Contenidos": list(queryset)})
	
	template = loader.get_template('LoVisto/aportaciones.html')
	context = {
		'user': request.user.username,
		'autenticado': request.user.is_authenticated,
		'last_aportaciones': Aportacion.objects.all().order_by('-fecha'),
		'last_3_aportaciones': Aportacion.objects.all().order_by('-fecha')[:3],
		'modo': modo,
	}
	
	return HttpResponse(template.render(context, request))

#Función para gestionar página de usuario	
@csrf_exempt
def usuario(request):
	autenticar(request)
	modo = 0
	
	if request.method == "POST":
		action = request.POST['action']
		if action == "Claro":
			modo = 0
		if action == "Oscuro":
			modo = 1	
	
	if request.GET.get("format")=="xml/":
		template = loader.get_template('LoVisto/user.xml')
		context = {
			'lista_aportaciones': Aportacion.objects.all().filter(user=request.user.username),
			'lista_votos': Voto.objects.all().filter(user=request.user.username),
			'comentarios': Comentario.objects.all().filter(user=request.user.username),
		}
		return HttpResponse(template.render(context, request))
		
	if request.GET.get("format")=="json/":
		queryset = Aportacion.objects.filter(user=request.user.username).values()
		queryset2 = Voto.objects.all().filter(user=request.user.username).values()
		queryset3 = Comentario.objects.all().filter(user=request.user.username).values()
		return JsonResponse({"Aportaciones realizadas": list(queryset),
					"Aportaciones en las que ha votado": list(queryset2),
					"Comentarios que ha realizado": list(queryset3)})	
				
	template = loader.get_template('LoVisto/user.html')
	context = {
		'user': request.user.username,
		'autenticado': request.user.is_authenticated,
		'lista_aportaciones': Aportacion.objects.all().filter(user=request.user.username),
		'lista_votos': Voto.objects.all().filter(user=request.user.username),
		'comentarios': Comentario.objects.all().filter(user=request.user.username),
		'last_3_aportaciones': Aportacion.objects.all().order_by('-fecha')[:3],
		'modo': modo,
	}
	return HttpResponse(template.render(context, request))

#Función para ver comentarios en xml y json
@csrf_exempt
def comentarios(request):
	if request.GET.get("format")=="xml/":
		template = loader.get_template('LoVisto/comentarios.xml')
		context = {
			'lista_comentarios': Comentario.objects.all(),
		}
		return HttpResponse(template.render(context, request))
	
	if request.GET.get("format")=="json/":
		queryset = Comentario.objects.all().values()
		return JsonResponse({"Comentarios": list(queryset)})

#Función para gestionar página de una aportación	
@csrf_exempt		
def aportacion(request, id):
	autenticar(request)
	modo = 0
			
	username = request.user.username
	user_aportaciones = Aportacion.objects.all().filter(user=username)
	user_aportaciones = user_aportaciones.order_by('-id')[:5]
	
	try:
		aportacion = Aportacion.objects.get(id=id)
	except Aportacion.DoesNotExist:
		context = {
			'aportaciones_list': Aportacion.objects.all(),
			'user': request.user.username,
			'autenticado': request.user.is_authenticated,
			'last_aportaciones': Aportacion.objects.all().order_by('-fecha')[:10],
			'user_aportaciones': user_aportaciones,
			'modo': modo,
		}
		return render(request, 'LoVisto/index.html', context)
	
	if request.GET.get("format")=="xml/":
		template = loader.get_template('LoVisto/aportacion.xml')
		context = {
			'aportacion': aportacion,
		}
		return HttpResponse(template.render(context, request))
	
	if request.GET.get("format")=="json/":
		queryset = Aportacion.objects.filter(id=id).values()
		return JsonResponse({"Contenidos": list(queryset)})	
		
	if request.method == "POST":
		action = request.POST['action']
		if action == 'Comentar':
			form = ComentarioForm(request.POST)
			if form.is_valid():
				c = Comentario(aportacion=aportacion, fecha=timezone.now(),user=request.user.username, texto=request.POST['texto'])
				c.save()
		elif action == 'Positivo':
			username=request.user.username
			votarPositivo(request, username, aportacion)
		elif action == 'Negativo':
			username=request.user.username
			votarNegativo(request, username, aportacion)
		if action == "Claro":
			modo = 0
		if action == "Oscuro":
			modo = 1	
	
					
	template = loader.get_template('LoVisto/aportacion.html')
	context = {
		'user': request.user.username,
		'autenticado': request.user.is_authenticated,
		'aportacion': aportacion,
		'comentarios': Comentario.objects.all().filter(aportacion=aportacion),
		'last_3_aportaciones': Aportacion.objects.all().order_by('-fecha')[:3],
		'modo': modo,
	}
	
	return HttpResponse(template.render(context,request))

#Función para gestionar página de información	
@csrf_exempt
def info(request):
	autenticar(request)
	modo = 0
	
	if request.GET.get("format")=="xml/":
		template = loader.get_template('LoVisto/info.xml')
		context = {}
		return HttpResponse(template.render(context, request))
	
	if request.GET.get("format")=="json/":
		template = loader.get_template('LoVisto/info.json')
		context = {}
		return HttpResponse(template.render(context, request))
	
	if request.method == "POST":
		action = request.POST['action']
		if action == "Claro":
			modo = 0
		if action == "Oscuro":
			modo = 1
	
	template = loader.get_template('LoVisto/info.html')
	context = {
		'user': request.user.username,
		'autenticado': request.user.is_authenticated,
		'last_3_aportaciones': Aportacion.objects.all().order_by('-fecha')[:3],
		'modo': modo,

	}
	return HttpResponse(template.render(context,request))

#Función para el recurso AEMET
def aemet(recurso):
	#Guardar xml y abrirlo
	xml = "https://www.aemet.es/xml/municipios/localidad_" + recurso + ".xml"
	handler = urllib.request.urlopen(xml)
	#Obtener los objetos python con los atributos del xml
	parser = xmltodict.parse(handler)
	lista_dias = []
	for dia in parser['root']['prediccion']['dia']:
		lista_dias.append("Estadísticas para el: " + dia['@fecha'])
		lista_dias.append("Temperaturas (min/max): " + dia['temperatura']['minima'] + "/" + dia['temperatura']['maxima'])
		lista_dias.append("Sensacion (min/max): " + dia['sens_termica']['minima'] + "/" + dia['sens_termica']['maxima'])
		lista_dias.append("Humedad relativa (min/max): " + dia['humedad_relativa']['minima'] + "/" + dia['humedad_relativa'][
                'maxima'])
		lista_dias.append("_._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._._.")

	dias = ""
	for dia in lista_dias:
		dias = dias + "<li>" + dia + "</li>"

	info_ext = "<p>Datos AEMET para " + parser['root']['nombre'] + "(" + parser['root']['provincia'] + ") </p><br>" + dias
	url_original = "http://www.aemet.es/es/eltiempo/prediccion/municipios/" + parser['root']['nombre'] + "-id" + recurso
	info_ext = "<br>" + info_ext + "<br><p>Copyright AEMET ---> <a href= '" + url_original + "'>Artículo original</a></p>"
	return info_ext	

#Función para el recurso YOUTUBE
def youtube(recurso):
	json_url = "https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v=" + recurso
	#Obtener toda la información
	url = requests.get(json_url)
	#Obtener los datos
	text = url.text
	datos = json.loads(text)
	#Guardar toda la información en el formato pedido
	info_ext = datos['title'] + "<br>" + datos['html'] + "<br>" + datos['author_name']
	return info_ext

#Función para el recurso NotaReddit
def notaReddit(recurso, subredit):
	json_url = "https://www.reddit.com/r/" + subredit + "/comments/" + recurso + "/.json"
	page = urllib.request.urlopen(json_url)
	#Obtener todos los datos
	text = page.read().decode('utf-8')
	datos = json.loads(text)
	#Sacar los datos por separado
	subreddit = datos[0]['data']['children'][0]['data']['subreddit']
	title = datos[0]['data']['children'][0]['data']['title']
	selftext = datos[0]['data']['children'][0]['data']['selftext']
	upvote = str(datos[0]['data']['children'][0]['data']['upvote_ratio'])
	url = datos[0]['data']['children'][0]['data']['url']
	
	if ("https://i.redd.it/") in url:
		info_ext = "<br>" + title + "<br>" + "<img src= '" + url + "' width='250' height='250'>" + "<br>" + "<a href= '" + url + "'>Publicado en: " + subreddit + "</a>" + "<br>" + "Aportación: " + upvote + "<br>"
	else:
		info_ext = "<br>" + title + "<br>" + selftext + "<br>" + "<a href= '" + url + "'>Publicado en: " + subreddit + "</a>" + "<br>" + "Aportación: " + upvote + "<br>"
		
	return info_ext


#Función para recursos no reconocidos
def recursoNoReconocido(url):
	info_ext = ""
	page = urllib.request.urlopen(url)
	if not page:
		info_ext = "Información extendida no disponible"
	else:
		soup = BeautifulSoup(page, 'html.parser')
		ogTitle = soup.find('meta', property='og:title')
		if ogTitle:
			#Si existe 'title' en las propiedades de og miramos que exista la imagen
			ogImage = soup.find('meta', property='og:image')
			if ogImage:
				imagen = ogImage['content']
				info_ext = "<br>" + ogTitle['content'] + "<br><img src= '" + imagen + "' width='250' height='150'>"
		
		elif not ogTitle:
			#No existe la propiedad 'title' de og, miramos si hay alǵun elemento 'title'
			hTitle = soup.title.string
			if hTitle:
				info_ext = hTitle
			else:
				info_ext = "Información extendida no disponible"
	return info_ext

#Función para gestionar página de inicio	
@csrf_exempt	
def index(request):
	autenticar(request)
	modo = 0

	if request.method == "POST":
		action = request.POST['action']
		if action == "Añadir aportacion":
			titulo = request.POST['titulo']
			descripcion = request.POST['descripcion']
			url = request.POST['url']
			#Comprobacion de recursos
			if ("aemet.es") in url:
				recurso = url.split("id")[1]
				info_ext = aemet(recurso)
				
			elif ("youtube.com") in url:
				recurso = url.split("=")[1]
				info_ext = youtube(recurso)
			elif ("reddit.com") in url:
				subredit = url.split("/")[4]
				recurso = url.split("/")[6]
				info_ext = notaReddit(recurso, subredit)
			else:
				if "http://" in url or "https://" in url:
					url = url
				else:
					url = "http://" + url	
				info_ext = recursoNoReconocido(url)
			try:
				aportacion = Aportacion.objects.get(url=url)
				aportacion.descripcion = descripcion
				aportacion.titulo = titulo
				aportacion.user = request.user.username
				aportacion.fecha = timezone.now()
				aportacion.num_votos_positivos = 0
				aportacion.num_votos_negativos = 0
				aportacion.info_extends=info_ext				
			except Aportacion.DoesNotExist:
				aportacion = Aportacion(titulo=titulo,descripcion=descripcion, url=url, fecha=timezone.now(), num_votos_positivos=0, num_votos_negativos=0, user=request.user.username, info_extends=info_ext)				
			aportacion.save()
	
		username = request.user.username
		if action == "Positivo":
			aportacion = Aportacion.objects.get(id=request.POST['id'])
			votarPositivo(request, username, aportacion)
		if action == "Negativo":
			aportacion = Aportacion.objects.get(id=request.POST['id'])
			votarNegativo(request, username, aportacion)
		if action == "Claro":
			modo = 0
		if action == "Oscuro":
			modo = 1
	
	#Obtener la lista con las últimas 5 aportaciones de un usuario
	username = request.user.username
	user_aportaciones = Aportacion.objects.all().filter(user=username)
	user_aportaciones = user_aportaciones.order_by('-id')[:5]
	
	#MÉTODO GET
	if request.GET.get("format")=="json/":
		queryset = user_aportaciones.values()
		queryset2 = Aportacion.objects.all().order_by('-fecha')[:10].values()
		return JsonResponse({"Últimas 10": list(queryset2),
					"Últimas 5 del usuario": list(queryset)})
	if request.GET.get("format")=="xml/":
		template = loader.get_template('LoVisto/index.xml')
		context = {
			'last_aportaciones': Aportacion.objects.all().order_by('-fecha')[:10],
		}
		return HttpResponse(template.render(context, request))
	template = loader.get_template('LoVisto/index.html')
	context = {
		'aportaciones_list': Aportacion.objects.all(),
		'user': request.user.username,
		'autenticado': request.user.is_authenticated,
		'last_aportaciones': Aportacion.objects.all().order_by('-fecha')[:10],
		'user_aportaciones': user_aportaciones,
		'last_3_aportaciones': Aportacion.objects.all().order_by('-fecha')[:3],
		'modo': modo,
	}
	
	return HttpResponse(template.render(context, request))


