from django.test import TestCase
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.test import Client

# Create your tests here.

class GetTests(TestCase):

	#Test de GET sobre la página principal
	@csrf_exempt
	def test_principal_page(self):
		client = Client()
		response = client.get('/lovisto/')
		self.assertEqual(response.status_code, 200)

		content = response.content.decode('utf-8')
		self.assertIn('Últimas 10 aportaciones:', content)
		
	#Test de GET sobre la página de todas las aportaciones
	@csrf_exempt
	def test_aportaciones_page(self):
		client = Client()
		response = client.get('/lovisto/aportaciones/')
		self.assertEqual(response.status_code, 200)

		content = response.content.decode('utf-8')
		self.assertIn('Bienvenido/a, estas son todas las aportaciones realizadas:', content)

	#Test de GET sobre la página de usuario (loggeado)	
	def test_user_page(self):
		client = Client()
		user = User.objects.create(username='user')
		user.set_password('11111')
		user.save()

		logged_in = client.login(username='user', password='11111')
		self.assertTrue(logged_in)

		response = client.get('/lovisto/usuario/')
		self.assertEqual(response.status_code, 200)
		content = response.content.decode('utf-8')
		self.assertIn("Aportaciones que ha realizado:", content)
	
	#Test de GET sobre la página de usuario (sin loggear)
	@csrf_exempt
	def test_user_page(self):
		client = Client()
		response = client.get('/lovisto/usuario/')
		self.assertEqual(response.status_code, 200)

		content = response.content.decode('utf-8')
		self.assertIn('Bienvenido/a', content)
		
	#Test de GET sobre la página de información	
	@csrf_exempt
	def test_info_page(self):
		client = Client()
		response = client.get('/lovisto/info/')
		self.assertEqual(response.status_code, 200)

		content = response.content.decode('utf-8')
		self.assertIn('Bienvenido/a a la página de información', content)
        
class PostTests(TestCase): 
	@csrf_exempt 
	def test_add_aportacion(self):
		cliente = Client()
		response = cliente.post('/lovisto/', {'action': "Añadir aportacion", 'titulo': "Lugo", 'url': "http://www.aemet.es/es/eltiempo/prediccion/municipios/lugo-id27028", 'descripcion': "Testeando con aemet"})
		
		self.assertEqual(response.status_code, 200)
		content = response.content.decode('utf-8')
		self.assertIn('Últimas 10 aportaciones:', content)
	
	@csrf_exempt 	
	def test_add_comentario(self):
		#Loggearse como usuario
		client = Client()
		user = User.objects.create(username='user')
		user.set_password('11111')
		user.save()
		
		logged_in = client.login(username='user', password='11111')
		self.assertTrue(logged_in)
		
		#Crear aportación
		response = client.post('/lovisto/', {'action': "Añadir aportacion", 'titulo': "Lugo", 'url': "http://www.aemet.es/es/eltiempo/prediccion/municipios/lugo-id27028", 'descripcion': "Testeando con aemet"})
		self.assertEqual(response.status_code, 200)
		
		#Añadir comentario
		response2 = client.post('/lovisto/aportacion/1', {'action': "Comentar", 'Comentario': "Primer comentario"})
		
		self.assertEqual(response.status_code, 200)
		content = response.content.decode('utf-8')
		self.assertIn('Puede visitar esta página en formato XML:', content)

	@csrf_exempt
	def test_voto_positivo(self):
		#Loggearse como usuario
		client = Client()
		user = User.objects.create(username='user')
		user.set_password('11111')
		user.save()
		
		logged_in = client.login(username='user', password='11111')
		self.assertTrue(logged_in)
		
		#Crear aportación
		response = client.post('/lovisto/', {'action': "Añadir aportacion", 'titulo': "Lugo", 'url': "http://www.aemet.es/es/eltiempo/prediccion/municipios/lugo-id27028", 'descripcion': "Testeando con aemet"})
		self.assertEqual(response.status_code, 200)
		
		#Añadir comentario
		response2 = client.post('/lovisto/aportacion/1', {'action': "Positivo", 'aportacion': '/lovisto/aportacion/1'})
		
		self.assertEqual(response.status_code, 200)
		#content = response.content.decode('utf-8')
		#self.assertIn('Puede visitar esta página en formato XML:', content)	
		
	@csrf_exempt
	def test_voto_negativo(self):
		#Loggearse como usuario
		client = Client()
		user = User.objects.create(username='user')
		user.set_password('11111')
		user.save()
		
		logged_in = client.login(username='user', password='11111')
		self.assertTrue(logged_in)
		
		#Crear aportación
		response = client.post('/lovisto/', {'action': "Añadir aportacion", 'titulo': "Lugo", 'url': "http://www.aemet.es/es/eltiempo/prediccion/municipios/lugo-id27028", 'descripcion': "Testeando con aemet"})
		self.assertEqual(response.status_code, 200)
		
		#Añadir comentario
		response2 = client.post('/lovisto/aportacion/1', {'action': "Negativo", 'aportacion': '/lovisto/aportacion/1'})
		
		self.assertEqual(response.status_code, 200)
		#content = response.content.decode('utf-8')
		#self.assertIn('Puede visitar esta página en formato XML:', content)	
	


  
     

