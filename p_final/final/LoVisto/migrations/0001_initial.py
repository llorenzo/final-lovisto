# Generated by Django 3.1.7 on 2021-06-11 08:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aportacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.TextField()),
                ('titulo', models.CharField(max_length=128)),
                ('descripcion', models.TextField()),
                ('url', models.TextField()),
                ('num_votos_positivos', models.IntegerField()),
                ('num_votos_negativos', models.IntegerField()),
                ('fecha', models.DateTimeField()),
                ('info_extends', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Voto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.TextField()),
                ('opcion', models.CharField(choices=[('P', 'POSITIVO'), ('N', 'NEGATIVO')], max_length=1)),
                ('aportacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LoVisto.aportacion')),
            ],
        ),
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.TextField()),
                ('user', models.TextField()),
                ('fecha', models.DateTimeField()),
                ('aportacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='LoVisto.aportacion')),
            ],
        ),
    ]
