from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Aportacion(models.Model):
	user = models.TextField()
	titulo = models.CharField(max_length=128)
	descripcion = models.TextField()
	url = models.TextField()
	num_votos_positivos = models.IntegerField()
	num_votos_negativos = models.IntegerField()
	fecha = models.DateTimeField()
	info_extends = models.TextField()
	
	def _str_(self):
		return str(self.id) + ": " + self.titulo 
	
class Comentario(models.Model):
	# Una aportacion puede tener varios comentarios (relación 1:N)
	aportacion = models.ForeignKey(Aportacion, on_delete = models.CASCADE)
	texto =  models.TextField()
	user = models.TextField()
	fecha = models.DateTimeField()
	
	def _str_(self):
		return str(self.id) + ": " + self.texto 
		
class Voto (models.Model):
	OPCIONES = [
		('P', 'POSITIVO'),
		('N', 'NEGATIVO')
	]
	# Una aportacion puede tener varios votos (relacion 1:N)
	aportacion = models.ForeignKey(Aportacion, on_delete=models.CASCADE)
	user = models.TextField()
	opcion = models.CharField(max_length=1, choices=OPCIONES)
	
	def _str_(self):
		return str(self.id) + ": " + self.opcion 
	

