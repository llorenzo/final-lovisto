from django.contrib import admin
from .models import Aportacion, Comentario, Voto

# Register your models here.
admin.site.register(Aportacion)
admin.site.register(Comentario)
admin.site.register(Voto)
