from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
	path('', views.index), 
	path('aportaciones/', views.aportaciones),
	path('usuario/', views.usuario),
	path('aportacion/<int:id>', views.aportacion),
	path('info/', views.info),
	path('comentarios/', views.comentarios), #Path solo para poder acceder a los comentarios en formato xml y json
]
