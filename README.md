# Final Lo Visto
Práctica final del curso 2020/21

# Entrega practica
## Datos
* Nombre: Lorena Álvarez Lorenzo 
* Titulación: Grado en Tecnologías de la Telecomunicación
* Video básico (url): https://www.youtube.com/watch?v=KtF8tB6lCN8
* Video parte opcional (url): https://www.youtube.com/watch?v=KPCe_nAVNGY
* Despliegue (url): http://lorenaalvarezlorenzo.pythonanywhere.com/lovisto/

## Cuenta Admin Site
* admin/admin

## Cuentas usuarios
* lorena/lorena
* pedro/pedro

## Resumen parte obligatoria
* He implementado toda la parte obligatoria del proyecto excepto pequeños detalles.

## Lista partes opcionales
* Inclusión de un favicon.
* XML y JSON de todas las páginas que se podrán descargar pinchando en los links que aparece en cada página.
* Comentarios en formato XML y JSON que se podrán descargar desde la página de todas las aportaciones mediante los links que aparecen.
